#include "instance.h"
#include "pxdip.h"
#include <libcuckoo/cuckoohash_map.hh>
#include <set>
#include <utility>

using namespace std;

namespace instance {
    cuckoohash_map<void*,int> instances_map;
}

class Instance::Priv {
public:
    uint32_t max_sz = 0;
    unique_ptr<uint16_t[]> s_pch;
    unique_ptr<uint16_t[]> s_ptv;
    unique_ptr<uint16_t[]> s_dst0;
    unique_ptr<uint16_t[]> s_dst1;
};

Instance::Instance() {
    p = make_unique<Priv>();
}

Instance::~Instance() {}

void Instance::resize(uint32_t max_px_count) {
    if (max_px_count > p->max_sz) {
        // 3x, because this will be used for planar store
        p->s_pch = make_unique<uint16_t[]>(max_px_count * 3);
        p->s_ptv = make_unique<uint16_t[]>(max_px_count * 3);
        // 6x, because resulting it is still planar, but this time, we have 2x more data
        p->s_dst0 = make_unique<uint16_t[]>(max_px_count * 6);
        p->s_dst1 = make_unique<uint16_t[]>(max_px_count * 6);
    }
}

const uint32_t Instance::get_current_max_px_count() {
    return p->max_sz;
}

uint16_t* Instance::get_pch() {
    return p->s_pch.get();
}

uint16_t* Instance::get_ptv() {
    return p->s_ptv.get();
}

uint16_t* Instance::get_dst0() {
    return p->s_dst0.get();
}

uint16_t* Instance::get_dst1() {
    return p->s_dst1.get();
}

//----- implementation of external instance interface -----
void* pxdip_init(const uint8_t max_thread) {
    auto ni = new Instance();
    instance::instances_map.insert(ni, 1);
    return ni;
}

void pxdip_destroy(const void* inst) {
    if (instance::instances_map.contains((void*)inst)) {
        instance::instances_map.erase((void*)inst);
        delete ((Instance*)inst);
    }
}
//----- end of implementation of external instance interface -----