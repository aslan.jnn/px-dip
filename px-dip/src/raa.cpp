#include "instance.h"
#include "pxdip.h"
#include <algorithm>
#include <cmath>

// DEBUG: include
#include <cstdio>

using namespace std;

namespace raapriv {

#define LIN_PCH(y, x) (((y) + 2) * pch_w + 2 + (x))
#define LIN_PCH_V(y) LIN_PCH(y, ix)
#define LIN_PTV LIN_PCH_V(iy)

    void vertical_tilt(uint16_t* __restrict__ pch,
        uint16_t* __restrict__ ptv,
        const uint16_t cp_w,
        const uint16_t cp_h) {
        // begin fn
        const uint16_t pch_w = cp_w + 4;
        // find the vertical tilt for each plannar channel. vertical tilt is also stored padded, as in pch
        for (int16_t iy = 0; iy < cp_h; ++iy) {
            for (int16_t ix = 0; ix < cp_w; ++ix) {
                uint16_t &m2c = pch[LIN_PCH_V(iy - 2)];
                uint16_t &m1c = pch[LIN_PCH_V(iy - 1)];
                uint16_t &cc = pch[LIN_PTV];
                uint16_t &p1c = pch[LIN_PCH_V(iy + 1)];
                uint16_t &p2c = pch[LIN_PCH_V(iy + 2)];

                const int16_t tbb = cc >= m1c ? cc - m1c : m1c - cc;
                const int16_t tcc = p1c >= cc ? p1c - cc : cc - p1c;
                const int16_t tc1 = p1c - m1c;
                const int16_t tc2 = m2c - p2c;
                const int16_t limit = min(cc < 128 ? cc : 0xFF ^ cc, (int)(tcc <= tbb ? tcc : tbb));
                const int16_t t = (3 * tc1 + tc2) / 8;
                ptv[LIN_PTV] = t > limit ? limit : (t < -limit ? -limit : t);
            }
        }
    }

#define LIN_PCH_H(x) LIN_PCH(iy, x)
    void horizontal_tilt(uint16_t* __restrict__ pch,
        uint16_t* __restrict__ ptv,
        uint16_t* __restrict__ dst0,
        uint16_t* __restrict__ dst1,
        uint16_t* __restrict__ dst2,
        uint16_t* __restrict__ dst3,
        const uint16_t cp_w,
        const uint16_t cp_h) {
        // begin fn
        const uint16_t pch_w = cp_w + 4;
        for (int16_t iy = 0; iy < cp_h; ++iy) {
            for (int16_t ix = 0; ix < cp_w; ++ix) {
                const uint16_t m2c = pch[LIN_PCH_H(ix - 2)];
                const uint16_t m1c = pch[LIN_PCH_H(ix - 1)];
                const uint16_t ccs = pch[LIN_PTV];
                const uint16_t tv = ptv[LIN_PTV];
                const uint16_t p1c = pch[LIN_PCH_H(ix + 1)];
                const uint16_t p2c = pch[LIN_PCH_H(ix + 2)];

                const int16_t tc1 = p1c - m1c;
                const int16_t tc2 = m2c - p2c;
                const int16_t t = (3 * tc1 + tc2) / 8;

                //stage-specific var declaration
                int16_t cc;
                int16_t tbb;
                int16_t tcc;
                int16_t limit;
                int16_t tt;

                //stage 1
                cc = ccs - tv;
                tbb = cc >= m1c ? cc - m1c : m1c - cc;
                tcc = p1c >= cc ? p1c - cc : cc - p1c;
                limit = min(cc < 128 ? cc : 0xFF ^ cc, (int)(tcc <= tbb ? tcc : tbb));
                tt = t > limit ? limit : (t < -limit ? -limit : t);
                dst0[LIN_PTV] = cc - tt;
                dst1[LIN_PTV] = cc + tt;

                //stage 2
                cc = ccs + tv;
                tbb = cc >= m1c ? cc - m1c : m1c - cc;
                tcc = p1c >= cc ? p1c - cc : cc - p1c;
                limit = min(cc < 128 ? cc : 0xFF ^ cc, (int)(tcc <= tbb ? tcc : tbb));
                tt = t > limit ? limit : (t < -limit ? -limit : t);
                dst2[LIN_PTV] = cc - tt;
                dst3[LIN_PTV] = cc + tt;
            }
        }
    }

    void to_planar(uint16_t* __restrict__ pch,
        const uint16_t cp_x,
        const uint16_t cp_y,
        const uint16_t cp_w,
        const uint16_t cp_h,
        const uint16_t src_w,
        const uint16_t src_h,
        uint32_t* __restrict__ src) {
#define BS(x) (((x) >> ch_shift) & 0xFF)
        const uint16_t pch_w = cp_w + 4;
        const uint16_t pch_h = cp_h + 4;
        const int16_t x_shift = max(0, 2 - cp_x);
        const int16_t y_shift = max(0, 2 - cp_y);
        uint16_t* __restrict__ tpch = pch;
        // loop bounds
        const int16_t bottom_iy_loop_end = (cp_h + cp_y + 2) - src_h;
        const int16_t right_ix_loop_end = (cp_w + cp_x + 2) - src_w;
        const int16_t mid_iy_loop_end = cp_h + min(src_h - cp_y - cp_h, 2);
        const int16_t mid_ix_loop_end = min(src_w - cp_x - cp_w, 2);
        for (uint32_t ch = 0, ch_shift = 0; ch < 3; ++ch, ch_shift += 8, tpch += pch_h * pch_w) {
            // top
            for (int16_t iy = 0; iy < y_shift; ++iy) {
                for (uint16_t ix = 0; ix < cp_w; ++ix) {
                    tpch[iy * pch_w + 2 + ix] = BS(src[cp_x + ix]);
                }
            }
            // bottom
            for (int16_t iy = 0; iy < bottom_iy_loop_end; ++iy) {
                for (uint16_t ix = 0; ix < cp_w; ++ix) {
                    tpch[(pch_h - 1 - iy) * pch_w + 2 + ix] = BS(src[(src_h - 1) * src_w + cp_x + ix]);
                }
            }
            // left
            for (int16_t ix = 0; ix < x_shift; ++ix) {
                for (uint16_t iy = 0; iy < cp_h; ++iy) {
                    tpch[(2 + iy) * pch_w + ix] = BS(src[(cp_y + iy) * src_w]);
                }
            }
            // right
            for (int16_t ix = 0; ix < right_ix_loop_end; ++ix) {
                for (uint16_t iy = 0; iy < cp_h; ++iy) {
                    tpch[(2 + iy) * pch_w + pch_w - 1 - ix] = BS(src[(cp_y + iy + 1) * src_w - 1]);
                }
            }
            // mid
            for (int16_t iy = y_shift - 2; iy < mid_iy_loop_end; ++iy) {
                for (int16_t ix = x_shift - 2; ix < cp_w + mid_ix_loop_end; ++ix) {
                    tpch[(iy + 2) * pch_w + ix + 2] = BS(src[(cp_y + iy) * src_w + cp_x + ix]);
                }
            }
        }
#undef BS
    }

    void flatten(uint16_t* __restrict__ p_dst,
        uint32_t* __restrict__ dst,
        int xoff,
        const uint16_t cp_x,
        const uint16_t cp_y,
        const uint16_t cp_w,
        const uint16_t cp_h,
        const uint16_t src_w) {
        const uint16_t pch_w = cp_w + 4;
        const uint16_t pch_h = cp_h + 4;
        for (int16_t iv = 0; iv < 2; ++iv) {
            for (int16_t iy = 0; iy < cp_h; ++iy) {
                for (int16_t ix = 0; ix < cp_w; ++ix) {
                    const uint32_t dst_p = ((2 * (cp_y + iy) + iv) * src_w + (cp_x + ix)) * 2;
                    const uint32_t nxt = pch_h * pch_w;
                    dst[dst_p + xoff] = 0xFF000000 | (p_dst[nxt * (iv * 3) + LIN_PTV]) |
                        (p_dst[nxt * (iv * 3 + 1) + LIN_PTV] << 8) | (p_dst[nxt * (iv * 3 + 2) + LIN_PTV] << 16);
                }
            }
        }
    }
}

void pxdip_raa(const void* inst,
    const uint16_t cp_x,
    const uint16_t cp_y,
    const uint16_t cp_w,
    const uint16_t cp_h,
    const uint16_t src_w,
    const uint16_t src_h,
    uint32_t* const src,
    uint32_t* const dst) {
    Instance* instance = (Instance*)inst;
    const int ptrjump = (cp_w + 4) * (cp_h + 4);
    // prepare for the worst
    instance->resize((src_w + 4) * (src_h + 4));
    raapriv::to_planar(instance->get_pch(), cp_x, cp_y, cp_w, cp_h, src_w, src_h, src);

    auto tpch = instance->get_pch();
    auto tptv = instance->get_ptv();
    auto tdst0 = instance->get_dst0();
    auto tdst1 = instance->get_dst1();
    for(unsigned int ch = 0; ch < 3; ++ch) {
        raapriv::vertical_tilt(tpch, tptv, cp_w, cp_h);
        raapriv::horizontal_tilt(tpch, tptv, tdst0, tdst1, tdst0 + (ptrjump * 3), tdst1 + (ptrjump * 3), cp_w, cp_h);
        tpch += ptrjump;
        tptv += ptrjump;
        tdst0 += ptrjump;
        tdst1 += ptrjump;
    }

    raapriv::flatten(instance->get_dst0(), dst, 0, cp_x, cp_y, cp_w, cp_h, src_w);
    raapriv::flatten(instance->get_dst1(), dst, 1, cp_x, cp_y, cp_w, cp_h, src_w);
}